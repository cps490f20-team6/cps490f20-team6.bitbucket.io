University of Dayton

Department of Computer Science

CPS 490 - Fall 2020

Dr. Phu Phung


# Capstone II Proposal


# Model Service Technology


# Team members



1.  Brandon Collins - collinsb9@udayton.edu
2.  Nick Mueller - muellern2@udayton.edu
3.  Jake Langenderfer - langenderferj1@udayton.edu
4.  Claire Gutsmiedl - gutsmiedl@udayton.edu


# Company Mentors

Rebecca Servaites  
Alex Graves  
Colin Leong  
Ian Cannon  


Company: University of Dayton Research Insititute


## Project homepage

<https://cps490f20-team6.bitbucket.io/>


# Overview

The overall structure is as follows: Submit a file to an API, store it in a database, create an endpoint that, when called, returns the file that was just uploaded. The endpoint must be permanent, as in, it cannot disappear when the docker container restarts.

![Overview Architecture](/assets/images/overviewArchitecture.png)



Figure 1 - An Overview Architecture of the proposed project.

# Project Context and Scope

This project once completed will be used by UDRI to access different models for AI related research. This is needed by UDRI becasuse it will allow them to bypass their current database system which uses complicated code and has several security risks. 

# High-level Requirements

Develop a service that is able to store models(files) in a database and create an API endpoint to retreive that model.

* Needs to be developed in Python
* The service needs to be modular (No direct interaction between the user and the database)
* The service needs to use a Swagger (RESTful) API
* Needs to be develeped with Gitlab or GitHub
* The endpoint must be permanent (can't disappear upon restart)
* The endpoint must take metadata about the file such as name, training data, and hyperparameters

# Technology

For our project, our team is programming in Python using the Visual Studios IDE and the Conda package management system. We are using a RESTful API, with Flask or Django as the web framework and Swagger as the Interface Description Language. Our team's project is done on a Linux operating system through Ubuntu 18. Our team's repository is residing on GitHub, and Docker containers are used to help deploy and run our project. Amazon S3 is the recommended storage service by our company. Our team is doing unit and functional testing to ensure that the program works properly.  

UDRI is providing our team with a skeleton project with some basic code that will include project structure, server functionality, API config file, and examples of unit and functionality tests. They are also providing some guidance on how to use the tools that are listed.


# Project Management

We will follow the Scrum approach, thus our team will identify the task in each sprint cycles, and have daily scrum meetings in the Spring semester. We will also have meetings 2 times a week for the remainder of the Fall Semester. The planned schedule and sprint cycles for Spring 2021 are as follows. 

![Spring 2021 Timeline](https://capstones-cs-udayton.bitbucket.io/imgs/cps491s21timeline.png "Spring 2021 Timeline")

 
Git Hub repository: https://github.com/collinsb9/cps490s20-team6  
Trello Board: https://trello.com/b/PA41xobq/cps490s21-team6  

Below is the Trello board timeline (Gantt chart) with sprint cycles for Spring 2021: 


![Spring 2021 Timeline on Trello](https://trello-attachments.s3.amazonaws.com/5faaef4088291f5ed2b09c02/5fd15a9bb9706b1d4930ed3b/c336e2cf489629b9f7888ab7e97d373d/springTrello.png "Spring 2021 Timeline")


# Company Support

Our mentors at UDRI will support us by being availible to answer questions and help resolve blockers as blockers come up. They will advise us through the development process and meet with us on a weekly basis on zoom. We will communicate through these weekly meetings and primarily by email. 